// This file is part of GNOME Games. License: GPL-3.0+.

private class Games.GameCubeRunner : RetroRunner {
	public GameCubeRunner (Game game, RetroCoreSource source) {
		base.from_source (game, source);
	}

	private bool is_dolphin () {
		var core = get_core ();

		var core_filename = core.get_filename ();
		var file = File.new_for_path (core_filename);
		var basename = file.get_basename ();

		return basename == "dolphin_libretro.so";
	}

	private string get_game_id () {
		var game = get_game ();
		var prefix = game.platform.get_uid_prefix ();
		var uid = game.uid.to_string ();

		return uid.substring (prefix.length + 1).ascii_up ();
	}

	private void inject_override (string save_dir_path, string name) throws Error {
		var file = File.new_for_uri (@"resource:///org/gnome/Games/plugins/game-cube/game-settings/$name.ini");

		if (!file.query_exists ())
			return;

		var dest_dir_path = Path.build_filename (save_dir_path, "User", "GameSettings");
		var dest_dir = File.new_for_path (dest_dir_path);

		if (!dest_dir.query_exists ())
			dest_dir.make_directory_with_parents ();

		var dest = dest_dir.get_child (file.get_basename ());

		if (dest.query_exists ())
			return;

		file.copy (dest, FileCopyFlags.NONE, null, null);

		debug ("Injecting game settings override: %s", dest.get_path ());
	}

	protected override void prepare_save_dir (string save_dir_path) throws Error {
		if (!is_dolphin ())
			return;

		var game_id = get_game_id ();

		inject_override (save_dir_path, game_id);
		inject_override (save_dir_path, game_id.substring (0, 3));
		inject_override (save_dir_path, game_id.substring (0, 1));
	}
}
