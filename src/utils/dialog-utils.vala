// This file is part of GNOME Games. License: GPL-3.0+.

namespace Games.DialogUtils {
	public async Gtk.ResponseType run_async (Gtk.Dialog dialog) {
		var response = Gtk.ResponseType.CANCEL;

		dialog.response.connect (r => {
			response = (Gtk.ResponseType) r;

			run_async.callback ();
		});

		dialog.present ();

		yield;

		return response;
	}

	public async Gtk.ResponseType run_native_async (Gtk.NativeDialog dialog) {
		var response = Gtk.ResponseType.CANCEL;

		dialog.response.connect (r => {
			response = (Gtk.ResponseType) r;

			run_native_async.callback ();
		});

		dialog.show ();

		yield;

		return response;
	}
}
